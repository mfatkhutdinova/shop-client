package main

import (
	context "context"

	scratch "gitlab.ozon.ru/platform/scratch"
	_ "gitlab.ozon.ru/platform/scratch/app/pflag"
	logger "gitlab.ozon.ru/platform/tracer-go/logger"

	shop_client_service "git.ozon.dev/mfatkhutdinova/shop-client/internal/app/shop-client-service"
	_ "git.ozon.dev/mfatkhutdinova/shop-client/internal/config"
)

func main() {
	a, err := scratch.New()
	if err != nil {
		logger.Fatalf(context.Background(), "can't create app: %s", err)
	}

	if err := a.Run(shop_client_service.NewShopClientService()); err != nil {
		logger.Fatalf(context.Background(), "can't run app: %s", err)
	}
}

