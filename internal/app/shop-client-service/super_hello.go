// Code generated by protoc-gen-goclay, but your can (must) modify it.
// source: shop-service-client.proto

package shopservice

import (
	"context"
	shopservice "git.ozon.dev/mfatkhutdinova/shop/pkg/shop-service"

	desc "git.ozon.dev/mfatkhutdinova/shop-client/pkg/shop-client-service"
)

func (i *Implementation) SuperHello(ctx context.Context, req *desc.SuperHelloRequest) (*desc.SuperHelloResponse, error) {
	resp, err := i.GetShopClient().Hello(ctx, &shopservice.HelloRequest{Name: req.GetName()})
	if err != nil {
		return nil, err
	}

	return &desc.SuperHelloResponse{Greeting: "Dear " + resp.GetGreeting() + "!"}, nil
}
